//
//  Allee_UITests.swift
//  AlleéUITests
//
//  Created by Daniel Junco on 3/29/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import XCTest

class Allee_UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        let loginButton = app.buttons["Login"]
        loginButton.tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("otavia@antiqua.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Daniel")
        loginButton.tap()
        app.buttons["aperture off"].tap()
        app.buttons["aperture on"].tap()
        
    }
    
    func testExample2() {
        
        let app = XCUIApplication()
        let loginButton = app.buttons["Login"]
        loginButton.tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("wiz@")
        emailTextField.tap()
        emailTextField.typeText("khalifa.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Daniel")
        loginButton.tap()
        
                
        
    }
    func testExample3(){
        
        let app = XCUIApplication()
        let signUpButton = app.buttons["Sign Up"]
        signUpButton.tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.tap()
        emailTextField.typeText("ordonez@wl.com")
        
        let choosePasswordSecureTextField = app.secureTextFields["Choose Password"]
        choosePasswordSecureTextField.tap()
        choosePasswordSecureTextField.typeText("Daniel")
        
        let repeatPasswordSecureTextField = app.secureTextFields["Repeat Password"]
        repeatPasswordSecureTextField.tap()
        repeatPasswordSecureTextField.typeText("Daniel")
        
        let dateOfBirthTextField = app.textFields["Date of Birth"]
        dateOfBirthTextField.tap()
        dateOfBirthTextField.tap()
        
        let doneButton = app.toolbars.buttons["Done"]
        doneButton.tap()
        app.textFields["Gender"].tap()
        doneButton.tap()
        app.textFields["Country"].tap()
        doneButton.tap()
        signUpButton.tap()
        
    }

    
}
