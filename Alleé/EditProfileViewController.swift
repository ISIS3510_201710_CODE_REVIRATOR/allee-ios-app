//
//  EditProfileViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/29/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Firebase
import AFNetworking
import FirebaseStorage
class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var displayNameText: UITextField!
    
    var databaseRef:FIRDatabaseReference!
    var storageRef:FIRStorageReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        databaseRef = FIRDatabase.database().reference()
        storageRef = FIRStorage.storage().reference()
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.clipsToBounds = true
        loadProfile()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    @IBAction func updateProfile(_ sender: Any) {
        let notif = Notification.Name.init(rawValue: "Ladies And Gentlemen We Are Floating In Space")
        editProfile()
        NotificationCenter.default.post(name: notif, object: nil)
        
    }
    
    func loadProfile(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            databaseRef.child("USER").child(userID).observe(.value, with: { (snapshot) in
                //Create a dictionary of users profile data
                let values = snapshot.value as? NSDictionary
                
                //If there is a URL image stored in photo
                if let profileImageURL = values?["photo"] as? String{
                    self.profileImageView.setImageWith(URL(string: profileImageURL)!)
                }
                self.displayNameText.text = values?["display"] as? String
            })
        }
    }
    @IBAction func changePhoto(_ sender: UIButton) {
        //Create instance of image picker controller 
        let picker = UIImagePickerController()
        //set Delegate
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //Holder for variable chosen image
        var chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImageView.image = chosenImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func editProfile(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            //Crete an acces point for the Firebase Storage
            let storageItem = storageRef.child("profile_images").child(userID).child(userID+".png")
            //get the image stored from the photo library
            guard let image = profileImageView.image else {return}
            if let newImage = UIImagePNGRepresentation(image){
                //upload to firebase storage
                storageItem.put(newImage, metadata: nil, completion: { (metadata, error) in
                    if error != nil{
                        print(error!)
                        return
                    }
                    storageItem.downloadURL(completion: { (url, error) in
                        if error != nil{
                            print(error!)
                            return
                        }
                        if let profilePhotoURL = url?.absoluteString{
                            guard let newDisplayName = self.displayNameText.text else {return}
                            
                            let newValuesForProfile = ["photo":profilePhotoURL+".png","display":newDisplayName]
                            //Update the firebase database 
                            self.databaseRef.child("Users").child(userID).updateChildValues(newValuesForProfile, withCompletionBlock: { (error, ref) in
                                if error != nil {
                                    print(error!)
                                    return
                                }
                                
                            })
                        }
                    })
                })
            }
            
        }
        self.dismiss(animated: true, completion: nil)
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func dismissPopUp(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}
