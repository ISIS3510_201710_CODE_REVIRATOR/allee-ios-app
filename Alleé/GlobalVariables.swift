//
//  GlobalVariables.swift
//  Alleé
//
//  Created by Daniel Junco on 3/6/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import Foundation
import UIKit

class GlobalVariables: NSObject {
    
    //Ultimo pais seleccionado en la tabla de paises
    var ultimoPaisSeleccionado: Pais? = nil
    var ultimoInteresSeleccionado: Interest? = nil
    
    static let sharedInstance = GlobalVariables()
    //Previene otras calses usar el '()' inicializador de esta clase.
    private override init() {}
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func displayAlertMessage(view: UIViewController, messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        view.present(alertController, animated: true, completion:nil)
    }

    
}

