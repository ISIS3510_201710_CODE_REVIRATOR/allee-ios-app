//
//  RateCountriesViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 4/2/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Firebase
import AFNetworking

var selectedCountries:[String] = [String]()

class RateCountriesViewController: UIViewController {
    
    var someInt: Int = 0
    var countries: [Pais] = [Pais]()
    var divisor: CGFloat!
    var databaseHandle:FIRDatabaseHandle?
    var ref: FIRDatabaseReference?
    var timerREST: Timer!
    var currentPais: Pais = Pais()
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var countryDescriptionText: UILabel!
    @IBOutlet weak var countryNameText: UILabel!
    @IBOutlet weak var card: DesignableView!
    @IBOutlet weak var thumbUpView: DesignableView!
    @IBOutlet weak var thumbDownView: DesignableView!
    
    
    
    @IBAction func penCard(_ sender: UIPanGestureRecognizer) {
        let card = sender.view!
        let point = sender.translation(in: view)
        card.center = CGPoint(x: view.center.x + point.x, y: view.center.y + point.y)
        let xFromCenter = card.center.x - view.center.x
        let scale = min(100 / abs(xFromCenter),1)
        card.transform = CGAffineTransform(rotationAngle: xFromCenter/divisor).scaledBy(x: scale, y: scale)
        if xFromCenter > 0 {
            thumbImageView.image = #imageLiteral(resourceName: "Thumb Up")
            thumbImageView.tintColor = UIColor.green
        }else{
            thumbImageView.image = #imageLiteral(resourceName: "Thumbs Down")
            thumbImageView.tintColor = UIColor.red
        }
        
        thumbImageView.alpha = abs(xFromCenter)/view.center.x
        
        if sender.state == UIGestureRecognizerState.ended{
            if card.center.x < 75 {
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x - 200 , y: card.center.y + 75)
                    card.alpha = 0
                })
                dislike()
                return
            }else if card.center.x > (view.frame.width - 75){
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x + 200 , y: card.center.y + 75)
                    card.alpha = 0
                })
                like()
                return
            }
            resetCard()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        divisor = (view.frame.width/2)/0.61
        numberOfCountries()
        timerREST = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getCountries), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        spinner.startAnimating()
        card.isHidden = true
        let navigationBarAppereance = self.navigationController!.navigationBar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        self.navigationItem.setHidesBackButton(true, animated: false)
        let color = GlobalVariables.sharedInstance.hexStringToUIColor(hex: "78E3FD")
        navigationBarAppereance.tintColor = color
    }
    
    @IBAction func thumUpAction(_ sender: UIButton) {
        like()
    }
    
    @IBAction func thumbDownAction(_ sender: UIButton) {
        dislike()
    }
    func resetCard(){
        UIView.animate(withDuration: 0.2) {
            self.card.center = self.view.center
            self.thumbImageView.alpha = 0
            self.card.alpha = 1
            self.card.transform = .identity
        }
    }
    
    func paintFirstCard(){
        spinner.stopAnimating()
        print(countries)
        card.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.thumbUpView.isHidden = false
            self.thumbDownView.isHidden = false
        }
        if !countries.isEmpty{
            currentPais = countries[0]
            self.countryNameText.text = currentPais.nombre
            self.imageView.setImageWith(URL(string: currentPais.imagen!)!)
            self.countryDescriptionText.text = currentPais.descripcion
        }
    }
    
    func paintCard(){
        if !countries.isEmpty{
            currentPais = countries[0]
            self.countryNameText.text = currentPais.nombre
            self.imageView.setImageWith(URL(string: currentPais.imagen!)!)
            self.countryDescriptionText.text = currentPais.descripcion
        }
        else{
            print("Terminamos aca")
            print(selectedCountries)
            doneTapped()
        }
    }
    
    func like(){
        if !countries.isEmpty {
            selectedCountries.append(currentPais.nombre!)
            _ = countries.removeFirst()
            UIView.animate(withDuration: 0.2, animations: {
                self.thumbImageView.alpha = 0
                self.card.alpha = 1
                self.card.transform = .identity
            })
            paintCard()
            print(selectedCountries)
        }
    }
    
    
    func dislike(){
        if !countries.isEmpty {
            _ = countries.removeFirst()
            UIView.animate(withDuration: 0.2, animations: {
                self.thumbImageView.alpha = 0
                self.card.alpha = 1
                self.card.transform = .identity
            })
            paintCard()
        }
    }
    
    func getCountries(){
        if countries.isEmpty{
            ref?.child("Countries").observe(.childAdded, with: { (snapshot) in
                self.currentPais = Pais(snapshot.value as! Dictionary<String, AnyObject>)
                self.countries.append(self.currentPais)
                if self.countries.count == self.someInt {
                    self.paintFirstCard()
                }
                
            })
        }
    }
    
    func numberOfCountries(){
        ref?.child("Countries").observe(.value, with: { (snapshot) in
            self.someInt = Int(snapshot.childrenCount)
        })
    }
    
    func doneTapped(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            ref?.child("Users").child(userID).child("SelectCountries").setValue(selectedCountries)
            performSegue(withIdentifier: "GoToLikes", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
