//
//  DesignableImageView.swift
//  Alleé
//
//  Created by Daniel Junco on 4/5/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableImageView: UIImageView {
    @IBInspectable var cornerRadius:CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
