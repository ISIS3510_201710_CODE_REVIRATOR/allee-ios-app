//
//  DesignableView.swift
//  Alleé
//
//  Created by Daniel Junco on 3/29/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
}
