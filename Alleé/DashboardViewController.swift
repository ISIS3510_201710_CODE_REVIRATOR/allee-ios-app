//
//  DashboardViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/23/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    
    @IBOutlet weak var placesButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var animatedButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var placesButtonCenter: CGPoint!
    var cameraButtonCenter: CGPoint!
    var profileButtonCenter: CGPoint!
    
    
    @IBAction func animatedButtonClicked(_ sender: UIButton) {
        if animatedButton.currentImage == #imageLiteral(resourceName: "aperture_off") {
            UIView.animate(withDuration: 0.3, animations: {
                //Animations Here
                self.cameraButton.alpha = 0.9
                self.profileButton.alpha = 0.9
                self.placesButton.alpha = 0.9
                self.cameraButton.center = self.cameraButtonCenter
                self.profileButton.center = self.profileButtonCenter
                self.placesButton.center = self.placesButtonCenter
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.cameraButton.alpha = 0
                self.profileButton.alpha = 0
                self.placesButton.alpha = 0
                self.cameraButton.center = self.animatedButton.center
                self.profileButton.center = self.animatedButton.center
                self.placesButton.center = self.animatedButton.center
            })
        }
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "aperture_on"), offImage: #imageLiteral(resourceName: "aperture_off"))
        
    }
    @IBAction func profileClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "user_on"), offImage: #imageLiteral(resourceName: "user_off"))
        performSegue(withIdentifier: "GoToProfile", sender: self)
    }
    
    @IBAction func cameraClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "camera_on"), offImage: #imageLiteral(resourceName: "camera_off"))
        performSegue(withIdentifier: "GoToCamera", sender: self)
    }
    
    @IBAction func placesClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "places_on"), offImage: #imageLiteral(resourceName: "places_off"))
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        placesButtonCenter = placesButton.center
        profileButtonCenter = profileButton.center
        cameraButtonCenter = cameraButton.center
        
        placesButton.center = animatedButton.center
        profileButton.center = animatedButton.center
        cameraButton.center = animatedButton.center
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = true
        searchBar.backgroundImage = UIImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toogleButton (button: UIButton, onImage:UIImage, offImage:UIImage){
        if button.currentImage == offImage{
            button.setImage(onImage, for: .normal)
        }else {
            button.setImage(offImage, for: .normal)
        }
    }
    
}
