//
//  LikesViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/22/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import AFNetworking
import FirebaseDatabase

class LikesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var interestsTableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var timerREST: Timer!
    var interests:[Interest] = [Interest]()
    var ref:FIRDatabaseReference?
    var databaseHandle: FIRDatabaseHandle?
    var selectedInterested: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        interestsTableView.delegate = self
        interestsTableView.dataSource = self
        interestsTableView.isHidden = true
        timerREST = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getInterests), userInfo: nil, repeats: false)
        
        //Set the firebase reference
        ref = FIRDatabase.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBarAppereance = self.navigationController!.navigationBar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        let color = GlobalVariables.sharedInstance.hexStringToUIColor(hex: "78E3FD")
        navigationBarAppereance.tintColor = color
    }
    
    func doneTapped () {
        performSegue(withIdentifier: "GoToDashBoard", sender: self)
    }
    
    func getInterests(){
        spinner.stopAnimating()
        interestsTableView.isHidden = false
        databaseHandle = ref?.child("Likes").observe(.childAdded, with: { (snapshot) in
            let currentInterest: Interest = Interest(snapshot.value as! Dictionary<String, AnyObject>)
            self.interests.append(currentInterest)
            self.interestsTableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "CellInterest", for:indexPath)
        
        //Se obtiene el pais en la posicion actual
        let currentInterest: Interest = interests[indexPath.row]
        
        //Obtenemos referencia a la vista de la imagen en la celda
        let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
        imageView.setImageWith(URL(string: currentInterest.imagen!)!)
        
        //Obtenemos referencia al campo de texto para el nombre
        let labelNombre: UILabel = cell.viewWithTag(2) as! UILabel
        labelNombre.text = currentInterest.nombre!
        
        //Obtenemos referencia al boton de love
        let loveButton: UIButton = cell.viewWithTag(3) as! UIButton
        loveButton.addTarget(self, action: #selector(CountriesViewController.loveActionButton(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellSelected:UITableViewCell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        cellSelected.backgroundColor = UIColor.purple
        //Obtenemos referencia a la vista de la imagen en la celda
        let imageView: UIImageView = cellSelected.viewWithTag(1) as! UIImageView
        imageView.alpha = 1
        let loveButton: UIButton = cellSelected.viewWithTag(3) as! UIButton
        loveButton.setImage(UIImage(named: "loveButtonRed"), for: .normal)
        loveButton.addTarget(self, action: #selector(self.loveActionButton(sender:)), for: .touchUpInside)
        let interestSeleccionado: Interest = interests[indexPath.row]
        if imageView.alpha == 1 {
            selectedInterested.append(interestSeleccionado.nombre!)
            print(selectedInterested)
        }
    }
    
    @IBAction func loveActionButton(sender: UIButton!) {
        sender.setImage(UIImage(named: "loveButtonRed"), for: .normal)
        sender.addTarget(self, action: #selector(self.retLoveActionButton(sender:)), for: .touchUpInside)
        
    }
    
    @IBAction func retLoveActionButton(sender: UIButton!){
        sender.setImage(UIImage(named: "loveButton2"), for: .normal)
        sender.addTarget(self, action: #selector(CountriesViewController.loveActionButton(sender:)), for: .touchUpInside)
    }
    


    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
