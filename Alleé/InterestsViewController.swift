//
//  InterestsViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/6/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import AFNetworking

class InterestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var interests:[Interest] = [Interest]()
    
    var timerREST: Timer!
    
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    
    
    @IBOutlet weak var tableViewInterests: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewInterests.delegate = self
        tableViewInterests.dataSource = self
        loadingSpinner.startAnimating()
        tableViewInterests.isHidden = true
        timerREST = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getInterests), userInfo: nil, repeats: false)
        
    }
    
    func getInterests() {
        loadingSpinner.stopAnimating()
        tableViewInterests.isHidden = false
        manager.get("/interests", parameters: nil, progress: { (progress) in
        }, success: { (task: URLSessionDataTask, response) in
            
            let arrayResponse: NSArray = response! as! NSArray
            
            for item in arrayResponse {
                let currentInterest: Interest = Interest (item as! Dictionary<String,
                    AnyObject>)
                
                self.interests.append(currentInterest)
                self.tableViewInterests.reloadData()
            }
        }) { (task: URLSessionDataTask?, error: Error) in
            print("Error task: \(task) -- Error Response: \(error) ")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "InterestCell", for:indexPath)
        
        //Obtenemos el pais en la posicion actual
        let currentInterest: Interest = interests[indexPath.row]
        
        //Obetenemos (con el tag) referenica a la vista de imagen en la celda
        let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
        imageView.setImageWith(URL(string: currentInterest.imagen!)!)
        
        //Obtenemos referencia al campo de texto para el nombre
        let labelNombre: UILabel = cell.viewWithTag(2) as! UILabel
        labelNombre.text = currentInterest.nombre!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let interestSeleccionado: Interest = interests[indexPath.row]
        GlobalVariables.sharedInstance.ultimoInteresSeleccionado = interestSeleccionado
        print(GlobalVariables.sharedInstance.ultimoInteresSeleccionado!.nombre!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerREST.invalidate()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}
