//
//  IntroViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/12/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import AVFoundation

class IntroViewController: UIViewController {

    
    var Player: AVPlayer!
    var PlayerLayer: AVPlayerLayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let URL = Bundle.main.url(forResource: "IntroVideo", withExtension: "mp4")
        Player = AVPlayer.init(url: URL!)
        
        PlayerLayer = AVPlayerLayer(player: Player)
        PlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        PlayerLayer.frame = view.layer.frame
        Player.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        Player.play()
        view.layer.insertSublayer(PlayerLayer, at: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: Player.currentItem)

    }
    
    func playerItemReachEnd(notification: NSNotification){
        Player.seek(to: kCMTimeZero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Player.pause()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Player.pause()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = true
        Player.play()
    }
    
    

    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
