//
//  Pais.swift
//  Alleé
//
//  Created by Daniel Junco on 3/6/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit

class Pais: NSObject {
    
    var nombre: String?
    var imagen: String?
    var descripcion: String?
    
    override init() {
        super.init()
    }
    
    convenience init(_ dictionary: Dictionary<String,AnyObject>){
        self.init()
        nombre = dictionary["Name"] as? String
        imagen = dictionary["URL"] as? String
        descripcion = dictionary["Description"] as? String
        
    }
}
