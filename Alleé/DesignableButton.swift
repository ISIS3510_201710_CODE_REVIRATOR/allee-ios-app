//
//  DesignableButton.swift
//  Alleé
//
//  Created by Daniel Junco on 3/28/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {

    @IBInspectable var cornerRadius:CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
