//
//  RegisterViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/12/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Navajo_Swift
import FirebaseAuth
import FirebaseDatabase

class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var signUpButton: UIButton!
    var ref:FIRDatabaseReference?
    var backTheme: String = "http://imgs.inkfrog.com/pix/trust2buy88/new_york-40.jpg"
    var profilePic: String = "http://jennstrends.com/wp-content/uploads/2013/10/bad-profile-pic-2.jpeg"
    
    @IBAction func repeatPassChange(_ sender: UITextField) {
        let pass = passwordFieldText.text
        let repeatPass = repeatPasswordField.text
        
        if (repeatPasswordField.text?.isEmpty)! {
            repeatPasswordLabel.isHidden = true
        }
        else if pass != "" && pass == repeatPass {
            repeatPasswordLabel.isHidden = false
            repeatPasswordLabel.textColor = UIColor.green
            repeatPasswordLabel.text = "Valid"
        }
        else{
            repeatPasswordLabel.isHidden = false
            repeatPasswordLabel.textColor = UIColor.red
            repeatPasswordLabel.text = "Don't Match with the Password"
        }
    }
    @IBOutlet weak var repeatPasswordLabel: UILabel!
    
    @IBAction func textEmailChange(_ sender: UITextField) {
        let providedEmailAddres = emailTextField.text
        let isEmailAddresValid = isValidEmailAddress(emailAddressString: providedEmailAddres!)
        if (emailTextField.text?.isEmpty)! {
            infoEmailLabel.isHidden = true
        }
        else if isEmailAddresValid {
            infoEmailLabel.isHidden = false
            infoEmailLabel.textColor = UIColor.green
            infoEmailLabel.text = "Valid"
        }
        else {
            infoEmailLabel.isHidden = false
            infoEmailLabel.textColor = UIColor.red
            infoEmailLabel.text = "Invalid"
        }
    }
    @IBOutlet weak var infoEmailLabel: UILabel!
    
    @IBAction func textFieldChange(_ sender: UITextField) {
        
        if (passwordFieldText.text?.isEmpty)!{
            passwordInfoLabel.isHidden = true
        }
            
        else if (passwordFieldText.text?.isEmpty)! == false {
            let lengthRule = NJOLengthRule(min: 6, max: 24)
            let lowercaseRule = NJORequiredCharacterRule(preset: .lowercaseCharacter)
            let uppercaseRule = NJORequiredCharacterRule(preset: .uppercaseCharacter)
            let validator = NJOPasswordValidator(rules: [lengthRule, uppercaseRule,lowercaseRule])
            
            if let failingRules = validator.validate(passwordFieldText.text!) {
                var errorMessages: [String] = []
                
                failingRules.forEach { rule in
                    errorMessages.append(rule.localizedErrorDescription)
                }
                passwordInfoLabel.isHidden = false
                passwordInfoLabel.textColor = UIColor.red
                passwordInfoLabel.text = errorMessages.joined(separator: "\n")
            } else {
                passwordInfoLabel.isHidden = false
                passwordInfoLabel.textColor = UIColor.green
                passwordInfoLabel.text = "Valid"
            }
        }         
    }
    @IBOutlet weak var passwordInfoLabel: UILabel!
    @IBOutlet weak var datePickerText: UITextField!
    @IBOutlet weak var genderPickerText: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!
    @IBOutlet weak var passwordFieldText: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    let datePicker = UIDatePicker()
    let genderPicker = UIPickerView()
    let countryPicker = UIPickerView()
    @IBOutlet weak var countryPickerText: UITextField!
    let genders = ["Male","Female"]
    let countries = ["Argentina", "Bolivia", "Brazil", "Canada", "China", "Colombia", "Ecuador", "England", "France","Germany", "Jamaica", "Japan", "Mexico", "United States", "Russia", "Venezuela"]

    override func viewDidLoad() {
        
        ref = FIRDatabase.database().reference()
        customPicker()
        createDatePicker()
        self.genderPicker.delegate = self
        self.genderPicker.dataSource = self
        self.countryPicker.delegate = self
        self.countryPicker.delegate = self
        super.viewDidLoad()
        passwordInfoLabel.isHidden = true
        infoEmailLabel.isHidden = true
        repeatPasswordLabel.isHidden = true
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = false
        navigationBarAppereance.barTintColor = UIColor.black
        navigationBarAppereance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationBarAppereance.tintColor = UIColor.white
        
        emailTextField.delegate = self
        passwordFieldText.delegate = self
        repeatPasswordField.delegate = self
        datePickerText.delegate = self
        genderPickerText.delegate = self
        
        navigationController!.toolbar.barTintColor = UIColor.black
        navigationController!.toolbar.isTranslucent = true
        navigationController!.toolbar.tintColor = UIColor.white
        //UINavigationBar.appearance().barTintColor = UIColor.red
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func createDatePicker() {
        
        //format date picker
        
        datePicker.datePickerMode = .date
        
        //toolbar 
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //barButtonItem
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedDate))
        toolbar.setItems([doneButton], animated: true)
        
        datePickerText.inputAccessoryView = toolbar
        datePickerText.inputView = datePicker
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = 0
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
    }
    
    func customPicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: true)
        genderPickerText.inputAccessoryView = toolbar
        countryPickerText.inputAccessoryView = toolbar
        genderPickerText.inputView = genderPicker
        countryPickerText.inputView = countryPicker
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == genderPicker {
            return genders[row]
        }
        else if pickerView == countryPicker {
            return countries[row]
        }
        else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPicker {
            self.genderPickerText.text = genders[0]
            return genders.count
        }
        else if pickerView == countryPicker {
            self.countryPickerText.text = countries[0]
            return countries.count
        }
        else{
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if pickerView == genderPicker {
            genderPickerText.text = genders[row]
        }
        else if pickerView == countryPicker {
            countryPickerText.text = countries[row]
        }
        
    }
    
    
    func donePressedDate(){
            //format date
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .none
            datePickerText.text = dateFormatter.string(from: datePicker.date)
            self.view.endEditing(true)
    }
    
    func donePressed(){
        
            self.view.endEditing(true)
        
    }
    
    @IBAction func signUpButton(_ sender: Any) {
        let delimeter = "@"
        let email = emailTextField.text
        let pass = passwordFieldText.text
        let rpass = repeatPasswordField.text
        let userName = email?.components(separatedBy: delimeter)
        let date = datePickerText.text
        let gender = genderPickerText.text
        let country = countryPickerText.text
        if ( (email?.isEmpty)! || (pass?.isEmpty)! || (rpass?.isEmpty)! || (date?.isEmpty)! || (gender?.isEmpty)! || (country?.isEmpty)! ){
            
            displayAlertMessage(messageToDisplay: "Please fill all the fields")
        }else if repeatPasswordLabel.text == "Valid" && infoEmailLabel.text == "Valid" && passwordInfoLabel.text == "Valid" {
            FIRAuth.auth()?.createUser(withEmail: email!, password: pass!, completion: { (user, error) in
                if error != nil {
                    self.displayAlertMessage(messageToDisplay: "\(String(describing: error))")
                }
                else{
                self.ref?.child("Users").child((user?.uid)!).setValue(["email":email,"userName":userName?[0],"fechaNacimiento":date,"genero":gender,"country":country,"display":userName?[0],"photo":self.profilePic ,"background":self.backTheme])
                    self.performSegue(withIdentifier: "GoToCountries", sender: self)
                    
                }
            })
        }
        else {
            displayAlertMessage(messageToDisplay: "Check your Internet Connection")
        }
        
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    
    
}
