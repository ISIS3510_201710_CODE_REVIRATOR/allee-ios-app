//
//  CameraViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/30/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class CameraViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var labelCoordenadas: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placesButtonCenter = placesButton.center
        profileButtonCenter = profileButton.center
        cameraButtonCenter = cameraButton.center
        
        placesButton.center = animatedButton.center
        profileButton.center = animatedButton.center
        cameraButton.center = animatedButton.center
    }
    
    @IBAction func takePhoto(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.allowsEditing = false
            
            self.present(imagePicker,animated: true, completion: nil)
        }else{
            noCameraAvailable()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        pictureImageView.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    func noCameraAvailable(){
        let alertNC = UIAlertController(title: "No camera available", message: "can't find a camera on this device", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertNC.addAction(okAction)
        present(alertNC, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save))
        let color = GlobalVariables.sharedInstance.hexStringToUIColor(hex: "78E3FD")
        navigationBarAppereance.tintColor = color
        navigationBarAppereance.barTintColor = UIColor.black
        navigationBarAppereance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationBarAppereance.tintColor = UIColor.white
        navigationBarAppereance.isTranslucent = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func save(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
        let storageRef = FIRStorage.storage().reference().child("user_images").child(userID).child(userID+String(Int(arc4random_uniform(1000)))+".png")
            if let uploadData = UIImagePNGRepresentation(self.pictureImageView.image!){
                storageRef.put(uploadData, metadata: nil, completion: { (metada, error) in
                    if error != nil {
                        print(error!)
                        return
                    }
                    storageRef.downloadURL(completion: { (url, error) in
                        if error != nil {
                            print(error!)
                            return
                        }
                        if let newPhotoURL = url?.absoluteString {
                            FIRDatabase.database().reference().child("Users").child(userID).child("photos").childByAutoId().setValue(newPhotoURL)
                        }
                        
                    })
                })
            }
            
        }
        GlobalVariables.sharedInstance.displayAlertMessage(view: self, messageToDisplay: "Se agrego la imagen")
        pictureImageView.image = UIImage()
    }

    @IBOutlet weak var placesButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var animatedButton: UIButton!
    
    var placesButtonCenter: CGPoint!
    var cameraButtonCenter: CGPoint!
    var profileButtonCenter: CGPoint!
    
    
    @IBAction func animatedButtonClicked(_ sender: UIButton) {
        if animatedButton.currentImage == #imageLiteral(resourceName: "aperture_off") {
            UIView.animate(withDuration: 0.3, animations: {
                //Animations Here
                self.cameraButton.alpha = 0.9
                self.profileButton.alpha = 0.9
                self.placesButton.alpha = 0.9
                self.cameraButton.center = self.cameraButtonCenter
                self.profileButton.center = self.profileButtonCenter
                self.placesButton.center = self.placesButtonCenter
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.cameraButton.alpha = 0
                self.profileButton.alpha = 0
                self.placesButton.alpha = 0
                self.cameraButton.center = self.animatedButton.center
                self.profileButton.center = self.animatedButton.center
                self.placesButton.center = self.animatedButton.center
            })
        }
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "aperture_on"), offImage: #imageLiteral(resourceName: "aperture_off"))
        
    }
    @IBAction func profileClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "user_on"), offImage: #imageLiteral(resourceName: "user_off"))
        UIView.animate(withDuration: 0.3, animations: {
            self.cameraButton.alpha = 0
            self.profileButton.alpha = 0
            self.placesButton.alpha = 0
            self.cameraButton.center = self.animatedButton.center
            self.profileButton.center = self.animatedButton.center
            self.placesButton.center = self.animatedButton.center
        })
        toogleButton(button: sender, onImage:#imageLiteral(resourceName: "user_off"), offImage: #imageLiteral(resourceName: "user_on"))
        toogleButton(button: animatedButton, onImage: #imageLiteral(resourceName: "aperture_off"), offImage: #imageLiteral(resourceName: "aperture_on"))
        performSegue(withIdentifier: "PicToProfile", sender: self)
    }
    
    @IBAction func cameraClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "camera_on"), offImage: #imageLiteral(resourceName: "camera_off"))
    }
    
    @IBAction func placesClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "places_on"), offImage: #imageLiteral(resourceName: "places_off"))
        UIView.animate(withDuration: 0.3, animations: {
            self.cameraButton.alpha = 0
            self.profileButton.alpha = 0
            self.placesButton.alpha = 0
            self.cameraButton.center = self.animatedButton.center
            self.profileButton.center = self.animatedButton.center
            self.placesButton.center = self.animatedButton.center
        })
        toogleButton(button: sender, onImage:#imageLiteral(resourceName: "places_off"), offImage: #imageLiteral(resourceName: "places_on"))
        toogleButton(button: animatedButton, onImage: #imageLiteral(resourceName: "aperture_off"), offImage: #imageLiteral(resourceName: "aperture_on"))
        performSegue(withIdentifier: "PicToPlaces", sender: self)
    }
    
    func toogleButton (button: UIButton, onImage:UIImage, offImage:UIImage){
        if button.currentImage == offImage{
            button.setImage(onImage, for: .normal)
        }else {
            button.setImage(offImage, for: .normal)
        }
    }


}
