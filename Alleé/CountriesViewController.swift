//
//  CountriesViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/22/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import AFNetworking
import FirebaseDatabase

var seleccion:[String] = [String]()

class CountriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  

    @IBAction func loveActionButton(sender: UIButton!) {
        if sender.currentImage == #imageLiteral(resourceName: "loveButton2"){
            sender.setImage(UIImage(named:"loveButtonRed"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named:"loveButton2"), for: .normal)
        }
        
    }

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var timerREST: Timer!
    @IBOutlet weak var paisesTableView: UITableView!
    var paises:[Pais] = [Pais]()
    
    var ref:FIRDatabaseReference?
    var databaseHandle: FIRDatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        paisesTableView.delegate = self
        paisesTableView.dataSource = self
        paisesTableView.isHidden = true
        timerREST = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getCountries), userInfo: nil, repeats: false)
        
        //Set the firebase reference
        ref = FIRDatabase.database().reference()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        let navigationBarAppereance = self.navigationController!.navigationBar
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        let color = GlobalVariables.sharedInstance.hexStringToUIColor(hex: "78E3FD")
        navigationBarAppereance.tintColor = color
    }
    
    func doneTapped () {
        performSegue(withIdentifier: "goToInterests", sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "CellPais", for:indexPath)
        
        //Se obtiene el pais en la posicion actual
        let currentPais: Pais = paises[indexPath.row]
        
        //Obtenemos referencia a la vista de la imagen en la celda
        let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
        imageView.setImageWith(URL(string: currentPais.imagen!)!)
        
        //Obtenemos referencia al campo de texto para el nombre
        let labelNombre: UILabel = cell.viewWithTag(2) as! UILabel
        labelNombre.text = currentPais.nombre!
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paises.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellSelected:UITableViewCell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        cellSelected.backgroundColor = UIColor.purple
        //Obtenemos referencia a la vista de la imagen en la celda
        let imageView: UIImageView = cellSelected.viewWithTag(1) as! UIImageView
        imageView.alpha = 1
        let loveButton: UIButton = cellSelected.viewWithTag(3) as! UIButton
        let paisSeleccionado: Pais = paises[indexPath.row]
        if loveButton.currentImage == #imageLiteral(resourceName: "loveButtonRed") {
            if !seleccion.contains(paisSeleccionado.nombre!){
                seleccion.append(paisSeleccionado.nombre!)
                print(seleccion)
            }
        }
    }
    

    
    func getCountries(){
        spinner.stopAnimating()
        paisesTableView.isHidden = false
        databaseHandle = ref?.child("Countries").observe(.childAdded, with: { (snapshot) in
            let currentPais: Pais = Pais(snapshot.value as! Dictionary<String, AnyObject>)
            self.paises.append(currentPais)
            self.paisesTableView.reloadData()
        })
    }
    
    func toogleButton (button: UIButton, onImage:UIImage, offImage:UIImage){
        if button.currentImage == offImage{
            button.setImage(onImage, for: .normal)
        }else {
            button.setImage(offImage, for: .normal)
        }
    }
    


}
