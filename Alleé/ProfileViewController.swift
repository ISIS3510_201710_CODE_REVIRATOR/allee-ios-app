//
//  ProfileViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/29/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Firebase

var numeroX: Int = 1

class ProfileViewController: UIViewController, UIPopoverControllerDelegate {

    @IBOutlet weak var profilePoints: UILabel!
    @IBOutlet weak var userNameText: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryText: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    var dataBaseRef: FIRDatabaseReference!
    
    override func viewWillAppear(_ animated: Bool) {
        
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = true
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.clipsToBounds = true
        profilePoints.isHidden = true
        userNameText.isHidden = true
        countryText.isHidden = true
        editButton.isHidden = true
        dataBaseRef = FIRDatabase.database().reference()
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            dataBaseRef.child("Users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
                let dictionary = snapshot.value as? NSDictionary
                
                let username = dictionary?["display"] as? String ?? "username"
                let country = dictionary?["country"] as? String ?? "country"
                if let countryURL = dictionary?["background"] as? String{
                    
                    let url = URL(string: countryURL)
                    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                        if error != nil {
                            print(error ?? "Occurrio un problema accediendo a tus fotos")
                            return
                        }
                        DispatchQueue.main.async {
                            self.countryImageView.image = UIImage(data: data!)
                        }
                    }).resume()
                }
                if let profileURL = dictionary?["photo"] as? String{
                    
                    let url = URL(string: profileURL)
                    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                        if error != nil {
                            print(error ?? "Occurrio un problema accediendo a tus fotos")
                            return
                        }
                        DispatchQueue.main.async {
                            self.profileImageView.image = UIImage(data: data!)
                        }
                    }).resume()
                }
                self.countryText.text = country
                self.userNameText.text = username
                self.profilePoints.isHidden = false
                self.editButton.isHidden = false
                self.countryText.isHidden = false
                self.userNameText.isHidden = false
                
            }) {(error) in
                print(error.localizedDescription)
                return
            }
        }
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notif = Notification.Name.init(rawValue: "Ladies And Gentlemen We Are Floating In Space")
        NotificationCenter.default.addObserver(self, selector: #selector(refreshList(notification:)), name: notif, object: nil)
        placesButtonCenter = placesButton.center
        profileButtonCenter = profileButton.center
        cameraButtonCenter = cameraButton.center
        
        placesButton.center = animatedButton.center
        profileButton.center = animatedButton.center
        cameraButton.center = animatedButton.center
            }
    
    func refreshList(notification: NSNotification){
        print("parent method is called")
        updateData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateData(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            dataBaseRef.child("USER").child(userID).observe( .childChanged, with: { (snapshot) in
                print(snapshot)
                let dictionary = snapshot.value as? NSDictionary
                
                let username = dictionary?["display"] as? String ?? "username"
                if let profileURL = dictionary?["photo"] as? String{
                    
                    let url = URL(string: profileURL)
                    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                        if error != nil {
                            print(error ?? "Occurrio un problema accediendo a tus fotos")
                            return
                        }
                        DispatchQueue.main.async {
                            self.profileImageView.image = UIImage(data: data!)
                        }
                    }).resume()
                }
                self.userNameText.text = username
                self.profilePoints.isHidden = false
                self.editButton.isHidden = false
                self.countryText.isHidden = false
                self.userNameText.isHidden = false
                
            }) {(error) in
                print(error.localizedDescription)
                return
            }
        }
    }
    
    @IBOutlet weak var placesButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var animatedButton: UIButton!
    
    var placesButtonCenter: CGPoint!
    var cameraButtonCenter: CGPoint!
    var profileButtonCenter: CGPoint!
    
    
    @IBAction func animatedButtonClicked(_ sender: UIButton) {
        if animatedButton.currentImage == #imageLiteral(resourceName: "aperture_off") {
            UIView.animate(withDuration: 0.3, animations: {
                //Animations Here
                self.cameraButton.alpha = 0.9
                self.profileButton.alpha = 0.9
                self.placesButton.alpha = 0.9
                self.cameraButton.center = self.cameraButtonCenter
                self.profileButton.center = self.profileButtonCenter
                self.placesButton.center = self.placesButtonCenter
            })
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.cameraButton.alpha = 0
                self.profileButton.alpha = 0
                self.placesButton.alpha = 0
                self.cameraButton.center = self.animatedButton.center
                self.profileButton.center = self.animatedButton.center
                self.placesButton.center = self.animatedButton.center
            })
        }
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "aperture_on"), offImage: #imageLiteral(resourceName: "aperture_off"))
        
    }
    @IBAction func profileClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "user_on"), offImage: #imageLiteral(resourceName: "user_off"))
    }
    
    @IBAction func cameraClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "camera_on"), offImage: #imageLiteral(resourceName: "camera_off"))
        UIView.animate(withDuration: 0.3, animations: {
            self.cameraButton.alpha = 0
            self.profileButton.alpha = 0
            self.placesButton.alpha = 0
            self.cameraButton.center = self.animatedButton.center
            self.profileButton.center = self.animatedButton.center
            self.placesButton.center = self.animatedButton.center
        })
        toogleButton(button: sender, onImage:#imageLiteral(resourceName: "camera_off"), offImage: #imageLiteral(resourceName: "camera_on"))
        toogleButton(button: animatedButton, onImage: #imageLiteral(resourceName: "aperture_off"), offImage: #imageLiteral(resourceName: "aperture_on"))
        performSegue(withIdentifier: "ProfToPic", sender: self)
    }
    
    @IBAction func placesClicked(_ sender: UIButton) {
        toogleButton(button: sender, onImage: #imageLiteral(resourceName: "places_on"), offImage: #imageLiteral(resourceName: "places_off"))
        UIView.animate(withDuration: 0.3, animations: {
            self.cameraButton.alpha = 0
            self.profileButton.alpha = 0
            self.placesButton.alpha = 0
            self.cameraButton.center = self.animatedButton.center
            self.profileButton.center = self.animatedButton.center
            self.placesButton.center = self.animatedButton.center
        })
        toogleButton(button: sender, onImage:#imageLiteral(resourceName: "places_off"), offImage: #imageLiteral(resourceName: "places_on"))
        toogleButton(button: animatedButton, onImage: #imageLiteral(resourceName: "aperture_off"), offImage: #imageLiteral(resourceName: "aperture_on"))
        performSegue(withIdentifier: "ProfToPlaces", sender: self)
    }
    
    func toogleButton (button: UIButton, onImage:UIImage, offImage:UIImage){
        if button.currentImage == offImage{
            button.setImage(onImage, for: .normal)
        }else {
            button.setImage(offImage, for: .normal)
        }
    }


}
