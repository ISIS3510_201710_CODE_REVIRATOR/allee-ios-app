//
//  SignInViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 3/21/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit

class SignInViewController: UIViewController, FBSDKLoginButtonDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var fbLoginButton: DesignableButton!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationBarAppereance = self.navigationController!.navigationBar
        navigationBarAppereance.isHidden = false
        navigationBarAppereance.barTintColor = UIColor.black
        navigationBarAppereance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationBarAppereance.tintColor = UIColor.white
        navigationController!.toolbar.barTintColor = UIColor.black
        navigationController!.toolbar.isTranslucent = true
        navigationController!.toolbar.tintColor = UIColor.white
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Entro")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            
        }
        print("Salir")
    }
    
    @IBAction func signIn(_ sender: Any) {
        if let email = emailTextField.text, let pass = passwordTextField.text {
            FIRAuth.auth()?.signIn(withEmail: email, password: pass, completion: { (user, error) in
                if let u = user {
                    self.performSegue(withIdentifier: "GoToDashBoard", sender: self)
                } else {
                    self.displayAlertMessage(messageToDisplay: "Incorrect Email/Password")
                }
            })
        }
        
    }
    
    @IBAction func handleFbLogin(_ sender: Any) {
        FBSDKLoginManager().logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Custom FB login failed: ", err ?? "")
                return
            }
            self.showEmailAddress()
        }
    }
    
    func showEmailAddress(){
        var name: String = ""
        var email: String = ""
        var country: String = ""
        var birthDay: String = ""
        var gender: String = ""
        var photo: String = ""
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else {
            return
        }
        let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString )
        FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
            if let u = user {
                self.performSegue(withIdentifier: "GoToDashboard", sender: self)
            }else {
                self.displayAlertMessage(messageToDisplay: "Something went wrong: " + "\(String(describing: error))")
            }
            
        })
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id,name,email,picture,birthday,first_name"]).start { (connection, result, err) in
            if err != nil {
                print("Failed to start graph request: ", err ?? "")
                return
            }
            print(result ?? "")
            let data:[String:AnyObject] = result as! [String : AnyObject]
            //name = data["name"] as! String
            //email = data["email"] as! String
            
        }
    }
    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    //Hide Keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    
}
