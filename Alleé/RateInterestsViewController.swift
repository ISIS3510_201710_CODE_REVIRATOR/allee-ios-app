//
//  RateInterestsViewController.swift
//  Alleé
//
//  Created by Daniel Junco on 4/5/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import UIKit
import Firebase
import AFNetworking

class RateInterestsViewController: UIViewController {

    var someInt: Int = 0
    var interests: [Interest] = [Interest]()
    var selectedInterests:[String] = [String]()
    var divisor: CGFloat!
    var databaseHandle:FIRDatabaseHandle?
    var ref: FIRDatabaseReference?
    var timerREST: Timer!
    var currentInterest: Interest = Interest()
    @IBOutlet weak var cardView: DesignableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!    
    @IBOutlet weak var countryNameText: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var thumbUpView: DesignableView!
    @IBOutlet weak var thumbDownView: DesignableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        divisor = (view.frame.width/2)/0.61
        numberOfInterests()
        timerREST = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(getInterests), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        spinner.startAnimating()
        cardView.isHidden = true
        let navigationBarAppereance = self.navigationController!.navigationBar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        self.navigationItem.setHidesBackButton(true, animated: false)
        let color = GlobalVariables.sharedInstance.hexStringToUIColor(hex: "78E3FD")
        navigationBarAppereance.tintColor = color
    }

    @IBAction func penCard(_ sender: UIPanGestureRecognizer) {
        let card = sender.view!
        let point = sender.translation(in: view)
        card.center = CGPoint(x: view.center.x + point.x, y: view.center.y + point.y)
        let xFromCenter = card.center.x - view.center.x
        let scale = min(100 / abs(xFromCenter),1)
        card.transform = CGAffineTransform(rotationAngle: xFromCenter/divisor).scaledBy(x: scale, y: scale)
        if xFromCenter > 0 {
            thumb.image = #imageLiteral(resourceName: "Thumb Up")
            thumb.tintColor = UIColor.green
        }else{
            thumb.image = #imageLiteral(resourceName: "Thumbs Down")
            thumb.tintColor = UIColor.red
        }
        
        thumb.alpha = abs(xFromCenter)/view.center.x
        
        if sender.state == UIGestureRecognizerState.ended{
            if card.center.x < 75 {
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x - 200 , y: card.center.y + 75)
                    card.alpha = 0
                })
                dislike()
                return
            }else if card.center.x > (view.frame.width - 75){
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x + 200 , y: card.center.y + 75)
                    card.alpha = 0
                })
                like()
                return
            }
            resetCard()
        }
    }
    
    func getInterests(){
        if interests.isEmpty{
            ref?.child("Likes").observe(.childAdded, with: { (snapshot) in
                self.currentInterest = Interest(snapshot.value as! Dictionary<String, AnyObject>)
                self.interests.append(self.currentInterest)
                if self.interests.count == self.someInt {
                    self.paintFirstCard()
                }
                
            })
        }
    }
    
    func numberOfInterests(){
        ref?.child("Likes").observe(.value, with: { (snapshot) in
            self.someInt = Int(snapshot.childrenCount)
        })
    }
    
    func resetCard(){
        UIView.animate(withDuration: 0.2) {
            self.cardView.center = self.view.center
            self.thumb.alpha = 0
            self.cardView.alpha = 1
            self.cardView.transform = .identity
        }
    }
    
    func paintFirstCard(){
        spinner.stopAnimating()
        print(interests)
        cardView.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.thumbUpView.isHidden = false
            self.thumbDownView.isHidden = false
        }
        if !interests.isEmpty{
            currentInterest = interests[0]
            self.countryNameText.text = currentInterest.nombre
            self.imageView.setImageWith(URL(string: currentInterest.imagen!)!)
        }
    }
    
    func paintCard(){
        if !interests.isEmpty{
            currentInterest = interests[0]
            self.countryNameText.text = currentInterest.nombre
            self.imageView.setImageWith(URL(string: currentInterest.imagen!)!)
        }
        else{
            print("Terminamos aca")
            print(selectedCountries)
            doneTapped()
        }
    }
    
    func like(){
        if !interests.isEmpty {
            selectedInterests.append(currentInterest.nombre!)
            _ = interests.removeFirst()
            UIView.animate(withDuration: 0.2, animations: {
                self.thumb.alpha = 0
                self.cardView.alpha = 1
                self.cardView.transform = .identity
            })
            paintCard()
            print(selectedInterests)
        }
    }
    
    
    func dislike(){
        if !interests.isEmpty {
            _ = interests.removeFirst()
            UIView.animate(withDuration: 0.2, animations: {
                self.thumb.alpha = 0
                self.cardView.alpha = 1
                self.cardView.transform = .identity
            })
            paintCard()
        }
    }
    
    @IBAction func thumbUpAction(_ sender: UIButton) {
        like()
    }
    
    @IBAction func thumbDownAction(_ sender: Any) {
        dislike()
    }
    func doneTapped(){
        if let userID = FIRAuth.auth()?.currentUser?.uid {
            ref?.child("Users").child(userID).child("Likes").setValue(selectedInterests)
            performSegue(withIdentifier: "GoToBoard", sender: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
