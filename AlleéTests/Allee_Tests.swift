//
//  Allee_Tests.swift
//  AlleéTests
//
//  Created by Daniel Junco on 3/28/17.
//  Copyright © 2017 Code Revirator. All rights reserved.
//

import XCTest

class Allee_Tests: XCTestCase {
    
    var paisesTest: [Pais] = [Pais]()
    var interestsTest: [Interest] = [Interest]()
    
    override func setUp() {
        super.setUp()
        let pais1 = Pais([
        "nombre":"Argentina" as AnyObject,
        "imagen":"URL1" as AnyObject
        ])
        
        let pais2 = Pais([
            "nombre":"Alemania" as AnyObject,
            "imagen":"URL2" as AnyObject
            ])
        
        let pais3 = Pais([
            "nombre":"Angola" as AnyObject,
            "imagen":"URL3" as AnyObject
            ])
        
        let pais4 = Pais([
            "nombre":"Australia" as AnyObject,
            "imagen":"URL4" as AnyObject
            ])
        paisesTest.append(pais1)
        paisesTest.append(pais2)
        paisesTest.append(pais3)
        paisesTest.append(pais4)
        
        let interest1 = Interest([
            "nombre":"Museos" as AnyObject,
            "imagen":"URL1" as AnyObject
            ])
        
        let interest2 = Interest([
            "nombre":"Paranormal" as AnyObject,
            "imagen":"URL2" as AnyObject
            ])
        
        let interest3 = Interest([
            "nombre":"Vida Nocturna" as AnyObject,
            "imagen":"URL3" as AnyObject
            ])
        
        let interest4 = Interest([
            "nombre":"Religion" as AnyObject,
            "imagen":"URL4" as AnyObject
            ])
        interestsTest.append(interest1)
        interestsTest.append(interest2)
        interestsTest.append(interest3)
        interestsTest.append(interest4)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInterests(){
        self.measure {
            var interesesProcesados = 0
            for interestTemp in self.interestsTest {
                print(interestTemp)
                interesesProcesados += 1
            }
            XCTAssert(interesesProcesados == 4, "Se esperaban 4 intereses procesados")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            var paisesProcesados = 0
            for paisTemp in self.paisesTest {
                print(paisTemp)
                paisesProcesados += 1
            }
            XCTAssert(paisesProcesados == 4, "Se esperaban 4 paises procesados")
        }
    }
    
    
    
}
